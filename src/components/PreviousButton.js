import React, { useState, useEffect } from 'react';
import styled from 'styled-components/macro';

const Prevb = styled.div`
    background: blue;
    width: 100px;
    height: 100px;
    position: absolute;
    left: 0;
    top: 0;
    background: url('icons/PREV_on.svg');
    background-repeat: no-repeat;
    background-size: cover;
`;

const PreviousButton = ({
    mainVideo,
    setMainVideo,
    reactionVideos,
    setReactionVideos,
    events, 
    setEvents,
    currentEventIndex,
    setCurrentEventIndex
}) => {
    return <Prevb onClick={() => {
        setCurrentEventIndex(currentEventIndex-1)
        setReactionVideos(events[currentEventIndex-1].reactions);
        setMainVideo({
            ...mainVideo,
            seekTime: events[currentEventIndex-1].time
        });
    }}/>;
};

export default PreviousButton;
