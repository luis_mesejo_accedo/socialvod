import React, { useState, useEffect } from 'react';
import styled from 'styled-components/macro';
import { VIDEO_STATUS } from '../constants';

const Nextb = styled.div`
width: 100px;
height: 100px;
position: absolute;
right: 0px;
top: 0;
background: url('icons/NEXT_on.svg');
background-repeat: no-repeat;
background-size: cover;
`;

const NextButton = ({
    mainVideo,
    setMainVideo,
    reactionVideos,
    setReactionVideos,
    events, 
    setEvents,
    currentEventIndex,
    setCurrentEventIndex
}) => {
    return <Nextb onClick={() => {
        setCurrentEventIndex(currentEventIndex+1)
        setReactionVideos(events[currentEventIndex+1].reactions);
        setMainVideo({
            ...mainVideo,
            seekTime: events[currentEventIndex+1].time
        });
    }}/>;
};

export default NextButton;
