import React, { useState, useEffect } from 'react';
import VideoPlayer from 'react-video-js-player';
import { VIDEO_STATUS } from '../constants';

const Player = ({
  video,
  setVideo,
  setseekTime
}) => {
  const [player, setPlayer] = useState(null);

  useEffect(() => {
    if (!video || !player ) return;
    if (video.status === VIDEO_STATUS.PLAY) player.play();
    if (video.status === VIDEO_STATUS.STOP) player.pause();
    if (video.seekTime !== 0) {
      setVideo({
        ...video,
        seekTime: 0,
      });
      player.currentTime(video.seekTime);
    }
  } ,[video, player, setVideo])

  const onPlayerReady = player => {
    console.log("Player is ready: ", player);
    setPlayer(player);
  }

  const onVideoPlay = duration => {
    console.log("Video played at: ", duration);
  }

  const onVideoPause = duration => {
    console.log("Video paused at: ", duration);
  }

  const onVideoTimeUpdate= duration => setseekTime(duration);

  const onVideoSeeking = duration => {
    console.log("Video seeking: ", duration);
  }

  const onVideoSeeked = (from, to) => {
    console.log(`Video seeked from ${from} to ${to}`);
  }

  const onVideoEnd = () => {
    console.log("Video ended");
  }

  if (!video) return null;

  return <VideoPlayer
    controls={false}
    src={video.url}
    poster={video.poster}
    width={video.width}
    height={video.height}
    onReady={player => onPlayerReady(player)}
    onPlay={duration => onVideoPlay(duration)}
    onPause={duration => onVideoPause(duration)}
    onTimeUpdate={duration => onVideoTimeUpdate(duration)}
    onSeeking={duration => onVideoSeeking(duration)}
    onSeeked={(from, to) => onVideoSeeked(from, to)}
    onEnd={() => onVideoEnd()}
  />;
};

export default Player;