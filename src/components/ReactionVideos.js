import React from 'react';
import styled from 'styled-components/macro';
import ReactionVideo from './ReactionVideo';

const ReactionVideosContainer = styled.div`
width: 100%;
height: 300px;
position: absolute;
top: 250px;
left: 100px;
`;

const ReactionVideos = ({
    setReactionVideos,
    reactionVideos,
    setseekTime
}) => <ReactionVideosContainer>
    <ReactionVideo 
    index={0}
    setReactionVideos={setReactionVideos}
    reactionVideos={reactionVideos}
    setseekTime={setseekTime}
    />
    <ReactionVideo 
    index={1}
    setReactionVideos={setReactionVideos}
    reactionVideos={reactionVideos}
    setseekTime={setseekTime}
    />
    <ReactionVideo 
    index={2}
    setReactionVideos={setReactionVideos}
    reactionVideos={reactionVideos}
    setseekTime={setseekTime}
    />

</ReactionVideosContainer>;

export default ReactionVideos;
