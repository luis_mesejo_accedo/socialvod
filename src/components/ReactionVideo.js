import React from 'react';
import styled from 'styled-components/macro';
import Player from './Player';
import PlayButton from './PlayButton';
import { VIDEO_STATUS } from '../constants';


const VideoContainer = styled.div`
    width: 500px;
    height: 300px;
    display: inline-block;
    position: relative;
    margin-right: 100px;
`;

const ReactionVideo = ({
    index,
    setReactionVideos,
    reactionVideos,
    setseekTime
}) => {
    return <VideoContainer>
        <Player video={reactionVideos[index]} setVideo={() => console.log('hey')} setseekTime={setseekTime}/>
        <PlayButton video={reactionVideos[index]} setVideo={newvideo => {
            const temparr = reactionVideos;
            temparr[index] = newvideo;
            setReactionVideos(temparr) }} top={80}/>
    </VideoContainer>;
};

export default ReactionVideo;
