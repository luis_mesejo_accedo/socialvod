import React, { useState, useEffect } from 'react';
import styled from 'styled-components/macro';
import { VIDEO_STATUS } from '../constants';

const Closeb = styled.div`
    background: white;
    width: 50px;
    height: 50px;
    position: absolute;
    top: 20px;
    left: 20px;
    border-radius: 25px;
`;

const CloseButton = ({
    timepos,
    settimepos
}) => {
    return <Closeb onClick={() => {
        if(timepos === 0 ) settimepos(-300);
        if(timepos === -300 ) settimepos(0);
    }}/>;
};

export default CloseButton;
