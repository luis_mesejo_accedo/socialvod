import React, { useState, useEffect } from 'react';
import styled from 'styled-components/macro';

const StartMark = styled.div`
    position: absolute;
    left: 0;
    top: 1px;
    background: white;
    width: 15px;
    height: 15px;
`;
const Bar = styled.div`
    background: white;
    position: absolute;
    width: 1920px;
    height: 10px;
    left: 40px;
`;
const TimeMark = styled.div`
    background: white;
    width: 10px;
    height: 10px;
    top: -5px;
    position: absolute;
    transform: rotate(45deg);
    left: ${props => 100+props.position/4}px;
`;

const Scrub = styled.div`
    background: white;
    width: 40px;
    height: 40px;
    position: absolute;
    top: -15px;
    border-radius: 40px; 
    left: ${props => 120+props.time/4}px;
`;

const EventIcon = styled.div`
    width: 50px;
    height: 50px;
    position: absolute;
    left: -50px;
    top: -50px;
    background: url('${props => props.icon}');
    background-repeat: no-repeat;
    background-size: cover;
`;

const EventBackground = styled.div`
    background: ${props => props.selected ? 'white' : 'transparent'}
    width: 50px;
    height: 50px;
    position: absolute;
    left: 0;
    top: 0;
`;
const TimeBar = ({
    mainVideo,
    setMainVideo,
    events,
    seekTime,
    currentEventIndex,
    setCurrentEventIndex,
    setReactionVideos
}) => {
    return <st>
        <Bar>
            <StartMark/>
            {events.map((event,i) => {
                return <>
                    <TimeMark position={event.time}>
                        <EventBackground position={event.time} selected={currentEventIndex === i}/>
                        <EventIcon onClick={() => {
                            setCurrentEventIndex(i);  
                            setMainVideo({
                                ...mainVideo,
                                seekTime: event.time,
                            });
                            setReactionVideos(event.reactions);
                        }} icon={event.icon}/>
                    </TimeMark>
                </>
            })}
        </Bar>
        <Scrub time={seekTime}/>
    </st>;
};

export default TimeBar;
