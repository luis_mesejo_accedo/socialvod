import React, { useState, useEffect } from 'react';
import styled from 'styled-components/macro';
import { VIDEO_STATUS } from '../constants';

const Playb = styled.div`
width: 100px;
height: 100px;
position: absolute;
left:  ${props => props.top ? 200 : 100 }px;
top: ${props => props.top ? 100 : 0 }px;
background: url('icons/PLAY_on.svg');
background-repeat: no-repeat;
background-size: cover;
`;

const PlayButton = ({
    video,
    setVideo,
    top
}) => {
    return <Playb top={top} onClick={() => setVideo({
        ...video,
        status: video.status === VIDEO_STATUS.PLAY ? VIDEO_STATUS.STOP : VIDEO_STATUS.PLAY
    })}/>;
};

export default PlayButton;
