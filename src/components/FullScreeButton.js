import React, { useState, useEffect } from 'react';
import styled from 'styled-components/macro';
import { VIDEO_STATUS } from '../constants';

const st = styled.div`
    background: green;
    width: 100px;
    height: 100px;
`;

const FullScreenButton = ({
    mainVideo,
    setMainVideo
}) => {
    return <st/>;
};

export default FullScreenButton;
