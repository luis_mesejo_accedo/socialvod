import React from 'react';
import styled from 'styled-components/macro';
import Player from './components/Player';

const VideoContainer = styled.div`
    width: 100%;
    height: 100%;
    background: black;
    position: absolute;
    top: 0;
    left: 0;
    z-index:-1;
`;

const MainVideo = ({
    mainVideo,
    setMainVideo,
    setseekTime
}) => {
    return <VideoContainer>
        <Player setseekTime={setseekTime} video={mainVideo} setVideo={setMainVideo}/>
    </VideoContainer>;
};

export default MainVideo;
