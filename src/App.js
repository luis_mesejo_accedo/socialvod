import React, { useState, useEffect } from 'react';
import { createGlobalStyle } from 'styled-components';
import styled from 'styled-components/macro';
import { BACKGROUND } from './style/colors';
import MainVideo from './MainVideo'
import Timeline from './Timeline'
import { VIDEO_STATUS, eventsVOD } from  './constants';
import CloseButton from './components/CloseButton';

const AppDiv = styled.div`
  position: relative;
  width: 100vw;
  height: 100vh;
`;

const GlobalStyle = createGlobalStyle`
  body {
    background-color: ${BACKGROUND};
    margin: 0;
    padding: 0;
    overflow: hidden;
    height: 100%;
    width: 100%;
  }
`;

const App = () => {
  const [mainVideo, setMainVideo] = useState(eventsVOD.MainVideo);
  const [seekTime, setseekTime] = useState(0);
  const [reactionVideos, setReactionVideos] = useState([]);
  const [events, setEvents] = useState(eventsVOD.Events);
  const [pipVideo, setPipVideo] = useState([]);
  const [currentEventIndex, setCurrentEventIndex] = useState(-1); 
  const [timepos, settimepos] = useState(0);

  return <>
    <GlobalStyle/>
    <AppDiv>
      <MainVideo mainVideo={mainVideo} setMainVideo={setMainVideo} setseekTime={setseekTime} />
      <Timeline 
      timepos={timepos}
      mainVideo={mainVideo}
      setMainVideo={setMainVideo}
      reactionVideos={reactionVideos}
      setReactionVideos={setReactionVideos}
      events={events}
      setEvents={setEvents}
      seekTime={seekTime}
      currentEventIndex={currentEventIndex}
      setCurrentEventIndex={setCurrentEventIndex}
      setseekTime={setseekTime}
      />
      <CloseButton timepos={timepos} settimepos={settimepos}/>
    </AppDiv>
  </>;
};

export default App;
