export const VIDEO_STATUS = {
    PLAY: 'play',
    STOP: 'stop'
};
export const EVENT_TYPES = {
    RED_CARD: 'redcard',
    GOAL: 'goal',
    HALF_TIME: 'halftime'
};

export const eventsVOD = {
    MainVideo: {
        url: 'mainvideo.mp4',
        width: 1920,
        height: 1080,
        seekTime: 0,
        status: VIDEO_STATUS.STOP,
    },
    Events: [{
        text: 'Penalty in Messi',
        time: 2366,//'00:39:26',
        icon: 'icons/red_card.svg',
        type: EVENT_TYPES.RED_CARD,
        reactions: [{
            autor: 'Inigo',
            videoName: 'Cheers in Campnou',
            duration: 120,
            url: 'reactions/01_A2.mp4',
            views: 12,
            likes: 10,
            tags: ['#PENALTY'],
            width: 500,
            height: 300,
            seekTime: 0,
            status: VIDEO_STATUS.STOP,
            filter: ['Barça']
        },
        {
            autor: 'Vicente',
            videoName: 'Fiesta de MADRID',
            duration: 0,
            url: 'reactions/02_REAL.mp4',
            views: 5,
            likes: 2,
            width: 500,
            height: 300,
            seekTime: 0,
            status: VIDEO_STATUS.STOP,
            tags: [],
            filter: ['BARÇA']
        },
        {
            autor: 'Aahd',
            videoName: 'NOT Penalty from',
            duration: 0,
            url: 'reactions/01_A3.mp4',
            views: 5,
            width: 500,
            height: 300,
            seekTime: 0,
            status: VIDEO_STATUS.STOP,
            likes: 1,
            tags: [],
            filter: ['Barça','Friends']
        }]
    },{
        name: 'Half Time',
        time: 3500,//'00:58:20',
        icon: 'icons/start_match.svg',
        type: EVENT_TYPES.HALF_TIME,
        reactions: [{
            autor: 'Luis',
            videoName: 'Fiesta de Barça (en catalan)',
            duration: 0,
            url: 'reactions/02_BARCA.mp4',
            views: 5,
            likes: 2,
            width: 500,
            height: 300,
            seekTime: 0,
            status: VIDEO_STATUS.STOP,
            tags: [],
            filter: ['BARÇA']
        },
        {
            autor: 'Niklas',
            videoName: 'VAMOSSSSSSSSSS',
            duration: 0,
            url: 'reactions/03_04.mp4',
            views: 502,
            likes: 12,
            width: 500,
            height: 300,
            seekTime: 0,
            status: VIDEO_STATUS.STOP,
            tags: [],
            filter: ['Twitter','']
        },
        {
            autor: 'Vicente',
            videoName: 'Fiesta de MADRID',
            duration: 0,
            url: 'reactions/02_REAL.mp4',
            views: 5,
            likes: 2,
            width: 500,
            height: 300,
            seekTime: 0,
            status: VIDEO_STATUS.STOP,
            tags: [],
            filter: ['BARÇA']
        }]
    },{
        name: 'Goal Pique',
        time: 4680,//'01:18:00',
        icon: 'icons/ball.svg',
        type: EVENT_TYPES.GOAL,
        reactions: [{
            autor: 'Alysson',
            videoName: 'Real Enfadados',
            duration: 0,
            url: 'reactions/03_REALANGRY.mp4',
            views: 502,
            likes: 12,
            width: 500,
            height: 300,
            seekTime: 0,
            status: VIDEO_STATUS.STOP,
            tags: [],
            filter: ['','']
        },
        {
            autor: 'Juanma',
            videoName: 'Gol de Barça',
            duration: 0,
            url: 'reactions/03_02.mp4',
            views: 502,
            likes: 12,
            width: 500,
            height: 300,
            seekTime: 0,
            status: VIDEO_STATUS.STOP,
            tags: [],
            filter: ['Barca','']
        },
        {
            autor: 'Daniel',
            videoName: 'YEahhhhhhhhh',
            duration: 0,
            url: 'reactions/03_03.mp4',
            views: 502,
            width: 500,
            height: 300,
            seekTime: 0,
            status: VIDEO_STATUS.STOP,
            likes: 12,
            tags: [],
            filter: ['','']
        },
        {
            autor: 'Niklas',
            videoName: 'VAMOSSSSSSSSSS',
            duration: 0,
            url: 'reactions/03_04.mp4',
            views: 502,
            width: 500,
            height: 300,
            seekTime: 0,
            status: VIDEO_STATUS.STOP,
            likes: 12,
            tags: [],
            filter: ['Twitter','']
        },
    ]
    },{
        name: 'Goal Benzema',
        time: 5040,//'01:24:00',
        icon: 'icons/ball.svg',
        type: EVENT_TYPES.GOAL,
        reactions: [{
            autor: 'Txema',
            videoName: 'VAMOOSSS',
            duration: 0,
            url: 'reactions/04_01.mp4',
            views: 110,
            width: 500,
            height: 300,
            seekTime: 0,
            status: VIDEO_STATUS.STOP,
            likes: 110,
            tags: [],
            filter: ['Madrid','Facebook']
        },
        {
            autor: 'Victor',
            videoName: 'Bernabeu',
            duration: 0,
            url: 'reactions/04_02.mp4',
            views: 120,
            width: 500,
            height: 300,
            seekTime: 0,
            status: VIDEO_STATUS.STOP,
            likes: 120,
            tags: [],
            filter: ['Madrid','Friends']
        }
    ]},{
        name: 'Goal Cristiano',
        time: 6390,//'01:46:30',
        icon: 'icons/ball.svg',
        type: EVENT_TYPES.GOAL,
        reactions: [{
            autor: 'Javier',
            videoName: 'Vamos Cris',
            duration: 0,
            url: 'reactions/05_01.mp4',
            views: 10,
            width: 500,
            height: 300,
            seekTime: 0,
            status: VIDEO_STATUS.STOP,
            likes: 20,
            tags: [],
            filter: ['Madrid','Friends']
        },
        {
            autor: 'Txema',
            videoName: 'VAMOOSSS',
            duration: 0,
            url: 'reactions/04_01.mp4',
            views: 110,
            likes: 110,
            width: 500,
            height: 300,
            seekTime: 0,
            status: VIDEO_STATUS.STOP,
            tags: [],
            filter: ['Madrid','Facebook']
        },
        {
            autor: 'Salvatore',
            videoName: 'El mejor del mundo',
            duration: 0,
            url: 'reactions/05_02.mp4',
            views: 1110,
            likes: 220,
            width: 500,
            height: 300,
            seekTime: 0,
            status: VIDEO_STATUS.STOP,
            tags: [],
            filter: ['Madrid','Friends']
        }]
    }
    ]
}