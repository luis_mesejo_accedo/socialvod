export const BRIGHT_TEXT = 'rgb(211, 211, 211)';
export const LIGHT_TEXT = 'rgb(174, 174, 174)';

export const PRIMARY = '#212121';
export const SECONDARY = '#303030';

export const BACKGROUND = '#303030';

export const HAYU_CORAL = '#df3559';
export const HAYU_CORAL_BRIGHTER = '#ff466b';
export const INACTIVE_OPTION = '#2f2f2f';
export const INACTIVE_OPTION_TEXT = 'rgb(174, 174, 174)';
export const LIST_ITEM_BACK = '#444444';
