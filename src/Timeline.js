import React from 'react';
import styled from 'styled-components/macro';
import PreviousButton from './components/PreviousButton';
import NextButton from './components/NextButton';
import PlayButton from './components/PlayButton';
import TimeBar from './components/TimeBar';
import ReactionVideos from './components/ReactionVideos';

const TimelineContainer = styled.div`
width: 100vw;
height: 60vh;
position: absolute;
bottom: ${props => props.timepos}px;
left:0;
`;

const VideoControls = styled.div`
width: 300px;
height: 100px;
position: absolute;
top: 100px;
left:800px;
`;


const Background = styled.div`
width: 100%;
height: 100%;
background: black;
opacity: 0.6;
position: absolute;
top: 0;
left:0;;
`;
const Timeline =React.memo(({
    mainVideo,
    setMainVideo,
    events,
    setEvents,
    reactionVideos,
    setReactionVideos,
    seekTime,
    currentEventIndex,
    setCurrentEventIndex,
    setseekTime,
    timepos
}) => (<TimelineContainer timepos={timepos}>
    <Background/>
    <TimeBar 
    events={events}
    setEvents={setEvents}
    seekTime={seekTime}
    setReactionVideos={setReactionVideos}
    mainVideo={mainVideo}
    setMainVideo={setMainVideo}
    currentEventIndex={currentEventIndex}
    setCurrentEventIndex={setCurrentEventIndex}
    />
    <VideoControls>
        <PreviousButton mainVideo={mainVideo}
            setMainVideo={setMainVideo}
            reactionVideos={reactionVideos}
            setReactionVideos={setReactionVideos}
            events={events}
            setEvents={setEvents}
            seekTime={seekTime}
            currentEventIndex={currentEventIndex}
            setCurrentEventIndex={setCurrentEventIndex}/>
        <PlayButton video={mainVideo} setVideo={setMainVideo}/>
        <NextButton mainVideo={mainVideo}
            setMainVideo={setMainVideo}
            reactionVideos={reactionVideos}
            setReactionVideos={setReactionVideos}
            events={events}
            setEvents={setEvents}
            seekTime={seekTime}
            currentEventIndex={currentEventIndex}
            setCurrentEventIndex={setCurrentEventIndex}/>
    </VideoControls>
    <ReactionVideos reactionVideos={reactionVideos} setReactionVideos={setReactionVideos} setseekTime={setseekTime}/>
</TimelineContainer>)
);

export default Timeline;
